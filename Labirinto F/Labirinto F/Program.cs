﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labirinto_F
{
    class Program
    {
        //Este metodo é chamado quando é necessario terminar o programa
        //Caso a pessoa pertenda continuar chamamos o metodo IniciarProfama
        
        static void Terminar()
        {
            Console.Clear();
            Console.WriteLine("O jogo terminou.");
            Console.WriteLine("Deseja jogar de novo? s n");
            string JogarNovo = String.Empty;
            JogarNovo = Console.ReadLine();
            switch (JogarNovo.ToUpper())
            {
                case "S":
                {
                    IniciarProframa();
                    break;
                }
                case "N":
                {
                    Console.WriteLine("Prima Qualquer tecla para sair.");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                }
            }
        }
        static void Main(string[] args)
        {
            //Quando o programa arraca é chamado o metudo iniciarproframa
            //Este é responsavel por todo o jogo
            IniciarProframa();

        }

        private static void IniciarProframa()
        {
            const int LIN = 15;
            const int COL = 30;

            string[,] labirinto = new string[LIN, COL]
            {

                {"\t╔", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "╗", },
                {"\t║", " ", " ", " ", " ", " ", " ", " ", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", " ", " ", " ", " ", "▓", "▓", "▓", "▓", "║", },
                {"\t║", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "║", },
                {"\t║", " ", "▓", " ", "▓", "m", "▓", "▓", "▓", "▓", "▓", "▓", " ", " ", " ", " ", " ", " ", " ", " ", " ", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "║", },
                {"\t║", " ", "▓", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", " ", " ", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", " ", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", " ", " ", " ", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", " ", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", "m", "▓", "▓", " ", "▓", "▓", " ", "m", " ", " ", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", " ", " ", " ", " ", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", "▓", "▓", "▓", "▓", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", "▓", "▓", "▓", "▓", " ", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "║", },
                {"\t║", "▓", "▓", "▓", "▓", "▓", " ", " ", " ", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", " ", "▓", " ", " ", " ", " ", " ", "║", },
                {"\t║", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", "▓", " ", " ", " ", "▓", "▓", "▓", " ", "S", },
                {"\t╚", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "═", "╝", },

            };

            ConsoleColor forecolor;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            forecolor = Console.ForegroundColor;
            
            bool sair = false;
            int comando;
            int vidas = 3;
            int moedas = 0;
            int l = 1;
            int c = 1;
            int cima = 38;
            int baixo = 40;
            int esquerda = 37;
            int direita = 39;
            string texto = string.Empty;
            string icon = "\u00A5";
            labirinto[l, c] = icon;

            while (sair == false)
            {
                Console.Clear();
                Console.CursorTop = 5;
                for (int i = 0; i < LIN; i++)
                {
                    for (int j = 0; j < COL; j++)
                    {
                        if (labirinto[i, j] == icon)
                        {

                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(labirinto[i, j]);
                            Console.ForegroundColor = forecolor;

                        }
                        else Console.Write(labirinto[i, j]);
                    }
                    Console.WriteLine();
                }

                Console.WriteLine("\n\n\tUse as setas para deslocar o boneco");
                Console.WriteLine("\n\t\t\t{0}", (char)24);
                Console.WriteLine("\n\t\t     {0}     {1}", (char)27, (char)26);
                Console.WriteLine("\n\t\t\t{0}", (char)25);
                Console.WriteLine("\tVidas:{0}", vidas);
                Console.WriteLine("\tMoedas:{0}", moedas);
                if (texto != string.Empty)
                {
                    Console.WriteLine(texto);
                    Console.ReadKey();
                    Terminar();
                }

                comando = Convert.ToInt32(Console.ReadKey().Key);
                // seta cima=38; seta baixo,=40; seta esquerda=37, seta direita=39

                if (comando == baixo)
                {
                    if (labirinto[(l + 1), c] == " " || labirinto[(l + 1), c] == "m")
                    {
                        if (labirinto[l + 1, c] == "m")
                        {
                            moedas++;
                        }
                        labirinto[l, c] = " ";
                        labirinto[++l, c] = icon;
                    }
                    else if (labirinto[l + 1, c] == "▓" || labirinto[l + 1, c] == "═")
                    {
                        vidas--;
                    }


                }
                if (comando == cima)
                {

                    if (labirinto[(l - 1), c] == " " || labirinto[(l - 1), c] == "m")
                    {
                        if (labirinto[l - 1, c] == "m")
                        {

                            moedas++;
                        }
                        labirinto[l, c] = " ";
                        labirinto[--l, c] = icon;
                    }
                    else if (labirinto[l - 1, c] == "▓" || labirinto[l - 1, c] == "═")
                    {
                        vidas--;
                    }
                }
                if (comando == direita)
                {

                    if (labirinto[l, (c + 1)] == " " || labirinto[l, (c + 1)] == "m" || labirinto[l, (c + 1)] == "S")
                    {
                        if (labirinto[l, (c + 1)] == "m")
                        {

                            moedas++;
                        }
                        else if (labirinto[l, (c + 1)] == "S")
                        {

                            texto = "Boaaaaa! Ganhou :)";

                        }

                        labirinto[l, c] = " ";
                        labirinto[l, ++c] = icon;
                    }
                    else if (labirinto[l, c + 1] == "▓" || labirinto[l, c + 1] == "║")
                    {
                        vidas--;
                    }


                }
                if (comando == esquerda)
                {

                    if (labirinto[l, (c - 1)] == " " || labirinto[l, (c - 1)] == "m")
                    {
                        if (labirinto[l, c - 1] == "m")
                        {

                            moedas++;
                        }
                        labirinto[l, c] = " ";
                        labirinto[l, --c] = icon;
                    }
                    else if (labirinto[l, c - 1] == "▓" || labirinto[l, c - 1] == "\t║")
                    {
                        vidas--;
                    }


                }
                if (vidas == 0)
                {
                    texto = "Fim do jogo";
                }

            }
        }
    }

}

